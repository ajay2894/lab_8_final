/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author Harsh Shah, Gurjot Kaur, Ajay Mohandas
 */
public class AnalysisHelper {

    public void userWithMostLikes() {
        Map<Integer, Integer> userLikecount = new HashMap<Integer, Integer>();

        Map<Integer, User> users = DataStore.getInstance().getUsers();
        //Counting total likes on comments done by each user
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikecount.containsKey(user.getId())) {
                    likes = userLikecount.get(user.getId());
                }
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for (int id : userLikecount.keySet()) {
            if (userLikecount.get(id) > max) {
                max = userLikecount.get(id);
                maxId = id;                         //calcluating user with maximum likes
            }
        }
        System.out.println("User with most likes: " + max + "\n" + users.get(maxId));
    }

    public void getFiveMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {
                //so as to get decending list
                return o2.getLikes() - o1.getLikes();
            }
        });
        //Loop for comments having most likes
        System.out.println("\n5 most liked comments: ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
        }
    }

    public void averageLikesPerComment() {

        int sumOfLikes = 0;
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());

        for (Comment c : commentList) {
            sumOfLikes += c.getLikes();
        }
        
        System.out.println("\nAverage Likes per comments: " + (double) sumOfLikes / commentList.size());
    }

    public void postWithMostLikedComments() {

        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> postLikecount = new HashMap<Integer, Integer>();

        for (Post p : posts.values()) {

            for (Comment c : p.getComments()) {
                int sumLikes = 0;
                if (postLikecount.containsKey(p.getPostId())) {
                    sumLikes = postLikecount.get(p.getPostId());
                }
                sumLikes += c.getLikes();
                postLikecount.put(p.getPostId(), sumLikes);
            }

        }

        int max = 0;
        int maxId = 0;
        for (int id : postLikecount.keySet()) {
            if (postLikecount.get(id) > max) {
                max = postLikecount.get(id);
                maxId = id;
            }
        }
        System.out.println("\nPost with most liked comments: " + posts.get(maxId));
    }

    public void postWithMostComments() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> postCommentcount = new HashMap<Integer, Integer>();

        for (Post p : posts.values()) {

            for (Comment c : p.getComments()) {
                int commentcount = 0;
                commentcount = p.getComments().size();
                postCommentcount.put(p.getPostId(), commentcount);
            }

        }

        int max = 0;
        int maxId = 0;
        for (int id : postCommentcount.keySet()) {
            if (postCommentcount.get(id) > max) {
                max = postCommentcount.get(id);
                maxId = id;
            }
        }
        System.out.println("\nPost with most comments: " + posts.get(maxId));

    }

    public void getFiveInactiveUserPost() {
        LinkedHashMap<Integer, Integer> useridPosts = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        int count = 0;
        for (Post p : posts.values()) {

            if (useridPosts.containsKey(p.getUserId())) {
                int countOfKey = useridPosts.get(p.getUserId());
                useridPosts.put(p.getUserId(), ++countOfKey);
            } else {
                count = 1;
                useridPosts.put(p.getUserId(), count);

            }
        }

        List<Integer> mapKeys = new ArrayList<>(useridPosts.keySet());
        List<Integer> mapValues = new ArrayList<>(useridPosts.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = useridPosts.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }

        }
        // System.out.println(sortedMap + "sorted ");
        ArrayList<Integer> userkeys = new ArrayList<Integer>(sortedMap.keySet());
        System.out.println("\nTop 5 Inactive Users Based on Posts\n");
        for (int x = 0; x < userkeys.size() && x < 5; x++) {

            System.out.println("User " + userkeys.get(x) + " First Name: " + userkeys.get(x) + " Last Name: " + userkeys.get(x) + "\r");
        }

    }

    public void getFiveInactiveUserComment() {
        LinkedHashMap<Integer, Integer> useridComments = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        int count = 0;
        for (Comment c : comments.values()) {

            if (useridComments.containsKey(c.getUserId())) {
                int countOfKey = useridComments.get(c.getUserId());
                useridComments.put(c.getUserId(), ++countOfKey);

            } else {
                count = 1;
                useridComments.put(c.getUserId(), count);

            }
        }

        List<Integer> mapKeys = new ArrayList<>(useridComments.keySet());
        List<Integer> mapValues = new ArrayList<>(useridComments.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = useridComments.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }

        }

        // System.out.println(sortedMap + "sorted ");
        ArrayList<Integer> userkeys = new ArrayList<Integer>(sortedMap.keySet());
        System.out.println("\nTop 5 Inactive Users Based on Comments \n");
        for (int x = 0; x < userkeys.size() && x < 5; x++) {

            System.out.println("User " + userkeys.get(x) + " First Name: " + userkeys.get(x) + " Last Name: " + userkeys.get(x) + "\r");
        }

    }

    public void getFiveInactiveUserOverall() {
        LinkedHashMap<Integer, Integer> commentOverall = new LinkedHashMap<Integer, Integer>();
        LinkedHashMap<Integer, Integer> postOverall = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        int count = 0;
        for (Comment c : comments.values()) {

            if (commentOverall.containsKey(c.getUserId())) {
                int countOfKey = commentOverall.get(c.getUserId());
                commentOverall.put(c.getUserId(), ++countOfKey);

            } else {
                count = 1;
                commentOverall.put(c.getUserId(), count);

            }
        }
        System.out.println("Comment Overall" + commentOverall);
        int count1 = 0;
        for (Post p : posts.values()) {

            if (postOverall.containsKey(p.getUserId())) {
                int countOfKey = postOverall.get(p.getUserId());
                postOverall.put(p.getUserId(), ++countOfKey);
            } else {
                count1 = 1;
                postOverall.put(p.getUserId(), count1);

            }
        }
        System.out.println("postOverall" + postOverall);

        Map<Integer, Integer> sortedMapComments = new TreeMap<Integer, Integer>(commentOverall);

        //System.out.println("Sorted comments as per keys" +sortedMapComments);
        Map<Integer, Integer> sortedMapPost = new TreeMap<Integer, Integer>(postOverall);

        //System.out.println("Sorted comments as per keys" +sortedMapPost);
        HashMap<Integer, Integer> temp = new HashMap<Integer, Integer>(commentOverall);
        for (int i : postOverall.keySet()) {
            if (temp.containsKey(i)) {
                temp.put(i, postOverall.get(i) + temp.get(i));
            } else {
                temp.put(i, postOverall.get(i));
            }
        }

        System.out.println("Sum of post and comments" + temp);

        List<Integer> mapKeys = new ArrayList<>(temp.keySet());
        List<Integer> mapValues = new ArrayList<>(temp.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = temp.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }

        }

        System.out.println("Sorted map based on overall" + sortedMap);
        ArrayList<Integer> userkeys = new ArrayList<Integer>(sortedMap.keySet());
        System.out.println("\nTop 5 Inactive Users Based on Overall \n");
        for (int x = 0; x < userkeys.size() && x < 5; x++) {

            System.out.println("User " + userkeys.get(x) + " First Name: " + userkeys.get(x) + " Last Name: " + userkeys.get(x) + "\r");
        }

    }

    public void getFiveProactiveUserOverall() {
        LinkedHashMap<Integer, Integer> commentOverall = new LinkedHashMap<Integer, Integer>();
        LinkedHashMap<Integer, Integer> postOverall = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        int count = 0;
        for (Comment c : comments.values()) {

            if (commentOverall.containsKey(c.getUserId())) {
                int countOfKey = commentOverall.get(c.getUserId());
                commentOverall.put(c.getUserId(), ++countOfKey);

            } else {
                count = 1;
                commentOverall.put(c.getUserId(), count);

            }
        }
        System.out.println("Comment Overall" + commentOverall);
        int count1 = 0;
        for (Post p : posts.values()) {

            if (postOverall.containsKey(p.getUserId())) {
                int countOfKey = postOverall.get(p.getUserId());
                postOverall.put(p.getUserId(), ++countOfKey);
            } else {
                count1 = 1;
                postOverall.put(p.getUserId(), count1);

            }
        }
        System.out.println("postOverall" + postOverall);

        Map<Integer, Integer> sortedMapComments = new TreeMap<Integer, Integer>(commentOverall);

        //System.out.println("Sorted comments as per keys" +sortedMapComments);
        Map<Integer, Integer> sortedMapPost = new TreeMap<Integer, Integer>(postOverall);

        //System.out.println("Sorted comments as per keys" +sortedMapPost);
        HashMap<Integer, Integer> temp = new HashMap<Integer, Integer>(commentOverall);
        for (int i : postOverall.keySet()) {
            if (temp.containsKey(i)) {
                temp.put(i, postOverall.get(i) + temp.get(i));
            } else {
                temp.put(i, postOverall.get(i));
            }
        }

        //System.out.println("Sum of post and comments" + temp);
        List<Integer> mapKeys = new ArrayList<>(temp.keySet());
        List<Integer> mapValues = new ArrayList<>(temp.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = temp.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }

        }

        //System.out.println("Sorted map based on overall" +sortedMap);
        ArrayList<Integer> userkeys = new ArrayList<Integer>(sortedMap.keySet());
        System.out.println("\nTop 5 Proactive Users Based on Overall \n");
        for (int x = 9; x < userkeys.size() && x >= 5; x--) {

            System.out.println("User " + userkeys.get(x) + " First Name: " + userkeys.get(x) + " Last Name: " + userkeys.get(x) + "\r");
        }

    }
}
